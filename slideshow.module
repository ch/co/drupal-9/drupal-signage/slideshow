<?php

/**
 * @file
 * Module functions for slideshow.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\Role;

/**
 * Implements hook_theme().
 */
function slideshow_theme() {
  return [
    'slideshow_slide_page' => [
      'variables' => [
        'node' => NULL,
        'view_mode' => 'full',
        'main_heading' => NULL,
        'text_content' => NULL,
        'slide_image' => NULL,
        'image_size' => NULL,
      ],
      'template' => 'slideshow-slide-page',
    ],
    'slideshow_movie' => [
      'variables' => [
        'view_mode' => 'full',
        'movie_file' => NULL,
        'preview_time' => '0',
      ],
      'template' => 'slideshow-movie',
    ],
    'slideshow_webpage' => [
      'variables' => [
        'view_mode' => 'full',
        'webpage_uri' => NULL,
      ],
      'template' => 'slideshow-webpage',
    ],
    'slideshow_slide_default' => [
      'variables' => ['node' => NULL],
      'template' => 'slideshow-slide-default',
    ],
    'slideshow_nocontent' => [
      'templates' => 'slideshow-nocontent',
    ],
  ];
}

/**
 * Implements hook_entity_queue_insert().
 *
 * Create a role to manage an entityqueue when one is created.
 */
function slideshow_entity_queue_insert(EntityInterface $entity) {
  $role_info = _slideshow_roles_generate_roleinfo($entity);

  $role = Role::load($role_info['id']);

  if (!$role) {
    $role = Role::create($role_info);

    $permissions = _slideshow_get_queue_editor_permissions($entity->id());
    foreach ($permissions as $permission) {
      $role->grantPermission($permission);
    }

    $role->save();
  }
}

/**
 * Implements hook_entity_queue_delete().
 *
 * Remove role for managing an entityqueue when one is deleted.
 */
function slideshow_entity_queue_delete(EntityInterface $entity) {
  $role_info = _slideshow_roles_generate_roleinfo($entity);
  $role = Role::load($role_info['id']);

  if ($role) {
    $users_with_role = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['roles' => $role_info['id']]);
    foreach ($users_with_role as $user) {
      $user->removeRole($role_info['id']);
      $user->save();
    }
    $role->delete();
  }
}

/**
 * Generate info about the role we expect to manage an entityqueue.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The queue entity.
 */
function _slideshow_roles_generate_roleinfo(EntityInterface $entity) {
  return [
    'id' => $entity->id() . '_manager',
    'label' => 'Manage queue ' . $entity->label(),
  ];
}

/**
 * Implements hook_page_attachments().
 */
function slideshow_page_attachments(array &$attachments) {
  $current_theme = \Drupal::theme()->getActiveTheme()->getName();
  if ($current_theme === 'claro') {
    $attachments['#attached']['library'][] = 'slideshow/claro-tweaks';
  }
}

/**
 * Implements hook_form_BASE_FORMID_alter().
 *
 * Show which screens a slide group is assigned to when editing the slide group
 * directly.
 */
function slideshow_form_entity_subqueue_form_alter(&$form, $form_state, $form_id) {
  $entity = $form_state->getFormObject()->getEntity();

  $screens_for_group = _slideshow_get_screens_with_group($entity->id());
  if ($screens_for_group) {
    $form_prefix = 'This group is assigned to: ' . implode(',  ', $screens_for_group);
  }
  else {
    $form_prefix = 'This group is not assigned to any screens';
  }

  $form['#prefix'] = $form_prefix;
}

/**
 * Implements hook_form_BASE_FORMID_alter().
 *
 * Add form element to easily allow editor to set slide groups when editing a
 * slide.
 */
function slideshow_form_node_form_alter(&$form, $form_state, $form_id) {

  $account = \Drupal::currentUser();
  $entity = $form_state->getFormObject()->getEntity();

  $available_slide_groups = _slideshow_get_editable_queues_for_user($account, $entity);

  $options = [];
  foreach ($available_slide_groups as $slide_group) {
    $screens_for_group = _slideshow_get_screens_with_group($slide_group->id());
    if ($screens_for_group) {
      $screen_label = 'Assigned to: ' . implode(',  ', $screens_for_group);
    }
    else {
      $screen_label = 'Not assigned to any screens';
    }

    $options[$slide_group->id()] = $slide_group->label() . " ($screen_label)";
  }

  asort($options);

  $default_values = _slideshow_filter_current_queues_for_entity($entity, $available_slide_groups);

  if ($options) {
    $form['slide_groups'] = [
      '#type' => 'checkboxes',
      '#title' => 'Slide groups',
      '#options' => $options,
      '#default_value' => $default_values,
    ];

    if (!$entity->isNew() && empty($default_values)) {
      $form['slide_groups']['warning'] = [
        '#markup' => '<em>Warning:</em> This slide is not currently assigned to any slide groups that you manage.',
      ];
    }

    $form['actions']['submit']['#submit'][] = '_slideshow_process_submitted_node_edit_form';
  }
  else {
    $form['slide_groups_placeholder'] = [
      '#markup' => 'There are no slide groups you can edit',
    ];
  }

}

/**
 * Implements hook_node_access().
 */
function slideshow_node_access(EntityInterface $node, string $op, AccountInterface $account) {

  if ($op == 'view' || $op == 'view label') {
    return \Drupal::service('slideshow.slide_access')->checkAccess($node, $op);
  }

  if ($op == 'update' || $op == 'delete') {
    $editable_queues = _slideshow_get_editable_queues_for_user($account, $node);
    $assigned_queues = _slideshow_get_all_queues_with_entity($node);

    $denied_queues = array_diff_key($assigned_queues, $editable_queues);
    return AccessResult::forbiddenIf(!empty($denied_queues), "forbidden by slideshow: " . implode(",", array_keys($denied_queues)));
  }

  return AccessResult::neutral();
}

/**
 * Get list of screens with a specified slide group assigned.
 *
 * @param string $slide_group
 *   Machine name of the slide group.
 *
 * @return array
 *   Array (possibly empty) of screen labels
 */
function _slideshow_get_screens_with_group(string $slide_group) {
  $config = \Drupal::config('slideshow.settings');

  $screens = $config->get('screens');

  $screen_names = [];

  foreach ($screens as $screen) {
    if (in_array($slide_group, $screen['slide_groups'])) {
      $screen_names[] = $screen['name'];
    }
  }

  return $screen_names;
}

/**
 * Get entityqueues that a user is allowed to update.
 *
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The user account to check.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity being edited.
 *
 * @return array
 *   Array of queues the user can edit, keyed by queue id.
 */
function _slideshow_get_editable_queues_for_user(AccountInterface $account, EntityInterface $entity) {
  $entity_queue_repository = \Drupal::service('entityqueue.repository');

  $editable_queues = [];

  $queues = $entity_queue_repository->getAvailableQueuesForEntity($entity);

  foreach ($queues as $queue) {
    $can_update = $queue->access('update', $account);
    if ($can_update) {
      $editable_queues[$queue->id()] = $queue;
    }
  }

  return $editable_queues;
}

/**
 * Form submit handler to update entityqueue assignments for an entity.
 */
function _slideshow_process_submitted_node_edit_form($form, $form_state) {
  $entity_manager = \Drupal::entityTypeManager();
  $account = \Drupal::currentUser();

  $entity = $form_state->getFormObject()->getEntity();

  $values = $form_state->getValue('slide_groups');

  $queue_change = FALSE;

  foreach ($values as $queue_id => $include_entity) {
    $queue = $entity_manager->getStorage('entity_subqueue')->load($queue_id);
    if ($queue->access('update', $account)) {

      if ($include_entity && !$queue->hasItem($entity)) {
        $queue->addItem($entity)->save();
        $queue_change = TRUE;
        \Drupal::messenger()->addMessage($entity->label() . " added to " . $queue->label() . " slide group.");
      }

      if (!$include_entity && $queue->hasItem($entity)) {
        $queue_change = TRUE;
        $queue->removeItem($entity)->save();
        \Drupal::messenger()->addMessage($entity->label() . " removed from " . $queue->label() . " slide group.");
      }

    }
  }

  $available_slide_groups = _slideshow_get_editable_queues_for_user($account, $entity);
  $current_slide_groups = _slideshow_filter_current_queues_for_entity($entity, $available_slide_groups);

  if (empty($current_slide_groups)) {
    \Drupal::messenger()->addWarning(t('This slide is not currently assigned to any slide groups that you manage.'));
  }

  if ($queue_change) {
    Cache::invalidateTags(['node_list']);
  }
}

/**
 * Get current queues that an entity is assigned to.
 *
 * Note that this is expected to be pre-filtered to only include the queues
 * available to the current editor.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity to check.
 * @param array $queues
 *   Array of queue objects, keyed by queue id.
 *
 * @return array
 *   Array of queue ids from $queues that the entity is currently in.
 */
function _slideshow_filter_current_queues_for_entity(EntityInterface $entity, array $queues) {
  $entity_manager = \Drupal::entityTypeManager();

  $current_queues = [];

  foreach ($queues as $id => $queue) {
    if ($entity_manager->getStorage('entity_subqueue')->load($id)->hasItem($entity)) {
      $current_queues[] = $id;
    }
  }

  return $current_queues;

}

/**
 * Get all queues that an entity is assigned to.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity being edited.
 *
 * @return array
 *   Array of subqueues the entity is assigned to, keyed by queue id.
 */
function _slideshow_get_all_queues_with_entity(EntityInterface $entity) {
  $assigned_queues = [];

  $entity_queue_repository = \Drupal::service('entityqueue.repository');
  $entity_manager = \Drupal::entityTypeManager();

  $queues = $entity_queue_repository->getAvailableQueuesForEntity($entity);

  foreach ($queues as $id => $queue) {
    if ($entity_manager->getStorage('entity_subqueue')->load($id)->hasItem($entity)) {
      $assigned_queues[$id] = $queue;
    }
  }

  return $assigned_queues;

}

/**
 * List of permissions that should be granted to queue managers.
 *
 * @param string $queue_name
 *   The name of the queue being managed.
 *
 * @return array
 *   Array of permissions
 */
function _slideshow_get_queue_editor_permissions(string $queue_name) {
  $permissions = [];
  $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('node');

  // Permissions related to managing content.
  // Note: if you update this list, also review the permissions assigned
  // to the slideshow_administrator role.
  foreach (array_keys($bundles) as $bundle) {
    $permissions = array_merge($permissions, [
      "create $bundle content",
      "revert $bundle revisions",
      "delete any $bundle content",
      "edit any $bundle content",
      "view $bundle revisions",
    ]);
  }

  // Allow editors to view slides of 'any' status rather than just 'active'.
  // Needed for the 'preview any' route.
  $permissions = array_merge($permissions, [
    'bypass slideshow scheduling_status restriction',
  ]);

  // Permissions related to managing queues.
  $permissions = array_merge($permissions, [
    "update $queue_name entityqueue",
    'manipulate entityqueues',
  ]);

  // Permissions related to (un)publishing content.
  $permissions = array_merge($permissions, [
    'override all published option',
    'view any unpublished content',
  ]);

  // misc. other permissions.
  $permissions = array_merge($permissions, [
    'access administration pages',
    'view the administration theme',
    'access content overview',
    'access toolbar',
  ]);

  return $permissions;
}

/**
 * Implements hook_local_tasks_alter().
 */
function slideshow_local_tasks_alter(array &$local_tasks) {
  // Check if the 'system.admin_content' task exists and modify its title.
  if (isset($local_tasks['system.admin_content'])) {
    $local_tasks['system.admin_content']['title'] = t('All slides');
  }
}

/**
 * Implements hook_form_views_exposed_form_alter().
 *
 * We provide two routes for current and expired content which are
 * generated from the core content view. We set the action destination
 * on the exposed search form so that searches don't redirect to the
 * default view display.
 */
function slideshow_form_views_exposed_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $current_route = \Drupal::routeMatch()->getRouteName();

  if (in_array($current_route, ['slideshow.current_content', 'slideshow.expired_content'])) {
    $form['#action'] = Url::fromRoute($current_route)->toString();
  }
}
