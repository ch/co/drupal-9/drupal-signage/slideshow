(function (Drupal, $) {
  Drupal.behaviors.SlideshowImageEffects = {
    attach: function (context, settings) {
      $(document).ready(function () {

        if(typeof(settings.imageeffects) !== 'undefined') {
          var blur = settings.imageeffects.blur;
          var opacity = settings.imageeffects.opacity;
          if(typeof(blur) !== 'undefined') {
            var blurfilter = 'blur(' + blur + 'px)';
            $('.image').css('filter', blurfilter);
          }

          if(typeof(opacity) !== 'undefined') {
            var bgcolor = 'rgba(255,255,255,' + opacity + ')';
            $('.image').css('opacity', opacity);
          }
        }

      });
    }
  };
})(Drupal, jQuery);
