(function ($, Drupal, once) {
  Drupal.behaviors.loadPreviewSlides = {
    attach: function (context, settings) {

			once('loadPreviewSlides', 'html', context).forEach(function() {

				var slideContainers = $('.slide-preview-container', context);
				var totalSlides = slideContainers.length;
        var timeoutDuration = 10000;
				var currentIndex = 0;

				function loadNextSlide() {
					if (currentIndex < totalSlides) {

            var container = $(slideContainers[currentIndex]);
            var placeholder = container.find('.iframe-placeholder');
            var url = container.data('preview-src-callback');

            $.ajax({
              url: url,
              type: 'GET',
              dataType: 'json',
              success: function(response) {
                placeholder.html(response.data);

                iframe = placeholder.find('iframe');
                var timeout = setTimeout(function() {
                  // if the timeout expires, clear the onload handler
                  // and move on.
                  iframe.off('load');
                  currentIndex++;
                  loadNextSlide();
                }, timeoutDuration);

                iframe.on('load', function() {
                  clearTimeout(timeout);
                  currentIndex++;
                  loadNextSlide();
                });
              }
            });
					}
				}
        
        // bootstrap loading the first slide.
        loadNextSlide();

			});
    }
  };
})(jQuery, Drupal, once);
