(function (Drupal, $) {
  Drupal.behaviors.NextSlide = {
    attach: function (context, settings) {
      // Wait for the page to load
      $(document).ready(function () {
        // Define the timeout value
        var timeout = settings.nextSlide.timeout;
        // Define the URL to redirect to
        var redirectUrl = settings.nextSlide.url;

        // Use setTimeout to redirect after the specified timeout
        setTimeout(function () {
          window.location.href = redirectUrl;
        }, timeout);
      });
    }
  };
})(Drupal, jQuery);
