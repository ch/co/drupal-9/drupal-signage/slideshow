(function (Drupal, $) {
  Drupal.behaviors.MovieNextSlide = {
    attach: function (context, settings) {
      // Wait for the page to load
      $(document).ready(function () {
        // Define the URL to redirect to
        var redirectUrl = settings.nextSlide.url;
        $('video').attr('onended', 'location.href=' + '\'' + redirectUrl + '\'');

      });
    }
  };
})(Drupal, jQuery);
