(function (Drupal, $) {
  Drupal.behaviors.SlideshowSlideProperties = {
    attach: function (context, settings) {
      $(document).ready(function () {
        if(typeof(settings.slideproperties) !== 'undefined') {
          var bgcolour = settings.slideproperties.bgcolour;
          var textcolour = settings.slideproperties.textcolour;
          var textboxcolour = settings.slideproperties.textboxcolour;

          if(typeof(bgcolour) !== 'undefined') {
            $('body').css('background-color', bgcolour);
          }

          if(typeof(textcolour) !== 'undefined') {
            $('body').css('color', textcolour);
          }

          if(typeof(textboxcolour) !== 'undefined') {
            $('.main-heading').css('background-color', textboxcolour);
            $('.text-content').css('background-color', textboxcolour);
          }
        }

      });
    }
  };
})(Drupal, jQuery);
