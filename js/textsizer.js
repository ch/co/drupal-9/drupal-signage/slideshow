(function (Drupal, $) {
  Drupal.behaviors.TextSizer = {
    attach: function (context, settings) {
      $(document).ready(function() {

    var contentFontSize = 6; // vh
    // Set minimum spacing based on 2% of the window height so it
    // looks the same with resizing
    var spacing = window.innerHeight * 0.02;

    var textNodes = $('.text-content');
    // Check if the middle boxes are too close, or fall off the end
    // of the page
    var overlap = function () {
      var slideBox = $('.slide');
      var slideBoxBoundary = slideBox[0].getBoundingClientRect();

      var headingBox = $('.main-heading');
      if(headingBox.length === 0) {
        headingBoxBottom = slideBoxBoundary.bottom;
      }
      else {
        // If we have no heading, act as if it were a zero-height box
        // at the top of the slide.
        headingBoxBottom = headingBox[0].getBoundingClientRect().top;
      }

      var contentBox = $('.text-content');
      if(contentBox.length === 0) {
        // If we have no contentBox, act as if were a zero-height box
        // at the bottom of the slide.
        contentBoxTop = slideBoxBoundary.bottom;
        contentBoxBottom = slideBoxBoundary.bottom;
      }
      else {
        contentBoxBoundary = contentBox[0].getBoundingClientRect();
        contentBoxTop = contentBoxBoundary.top
        contentBoxBottom = contentBoxBoundary.bottom;
      }

      return ((window.innerHeight - contentBoxBottom < spacing)
        || (contentBoxTop - headingBoxBottom < spacing ));
    };
    // Scale boxes down until they fit, anything over 30 lines
    // looks stupid anyway so stop then
    for (var scaleFactor = 7; scaleFactor < 30; scaleFactor++) {
        for (var i = 0; i < textNodes.length; i++) {
          textNodes[i].style.fontSize = (7 * contentFontSize / scaleFactor) + "vh";
        }
        // Always rescale at least once to allow scaling up and down
        if (!overlap()) break;
      }
      });
    }
  };
})(Drupal, jQuery);
