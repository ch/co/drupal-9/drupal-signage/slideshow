(function (Drupal, $) {
  Drupal.behaviors.FadeIn = {
    attach: function (context, settings) {
      // Wait for the page to load
      $(document).ready(function () {
        $('.slide').fadeIn(1500);
      });
    }
  };
})(Drupal, jQuery);
