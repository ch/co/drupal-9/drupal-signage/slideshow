<?php

namespace Drupal\slideshow\Plugin\field_group\FieldGroupFormatter;

use Drupal\field_group\Plugin\field_group\FieldGroupFormatter\Tab;

/**
 * Plugin implementation of the 'tab' formatter.
 *
 * @FieldGroupFormatter(
 *   id = "slide_tab",
 *   label = @Translation("Slide Tab"),
 *   description = @Translation("This fieldgroup renders the content as a tab."),
 *   format_types = {
 *     "open",
 *     "closed",
 *   },
 *   supported_contexts = {
 *     "form",
 *   }
 * )
 */
class SlideTab extends Tab {

  /**
   * Field formatter process function.
   *
   * We want to append a visual cue to the tab label to indicate that the
   * tab contents are non-empty. The meaning of "non-empty" needs to be
   * determined on a case-by-case basis.
   *
   * @param array $element
   *   The field group render array.
   * @param object $processed_object
   *   The object / entity beïng processed.
   */
  public function process(&$element, $processed_object) {
    parent::process($element, $processed_object);

    if ($this->elementIsSet($element, $processed_object)) {
      $element['#title'] .= ' &bull;';
    }

  }

  /**
   * Check if a form element is set.
   *
   * @param array $element
   *   Render array.
   * @param object $processed_object
   *   The object being processed.
   *
   * @return bool
   *   True if we consider the element to have a value, false otherwise.
   */
  protected function elementIsSet($element, $processed_object): bool {
    $group_name = $element['#group_name'];

    $handlers = [
      'group_slide_content' => 'contentIsSet',
      'group_image' => 'imageIsSet',
      'group_movie' => 'movieIsSet',
      'group_webpage' => 'webpageIsSet',
      'group_scheduling' => 'schedulingIsSet',
      'group_publishing' => 'publishingIsSet',
    ];

    if (isset($handlers[$group_name])) {
      $handler = $handlers[$group_name];
      return $this->$handler($element, $processed_object);
    }

    // Default fallback if no handler is set.
    return FALSE;

  }

  /**
   * Check if slide content group is set.
   *
   * @param array $element
   *   Render array.
   * @param object $processed_object
   *   The object being processed.
   *
   * @return bool
   *   True if we consider the element to have a value, false otherwise.
   */
  protected function contentIsSet($element, $processed_object): bool {
    $has_heading = !empty($processed_object['field_main_heading']['widget'][0]['value']['#default_value']);
    $has_text = !empty($processed_object['field_text_content']['widget'][0]['value']['#default_value']);

    if ($has_heading || $has_text) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Check if image group is set.
   *
   * @param array $element
   *   Render array.
   * @param object $processed_object
   *   The object being processed.
   *
   * @return bool
   *   True if we consider the element to have a value, false otherwise.
   */
  protected function imageIsSet($element, $processed_object): bool {
    $has_image = !empty($processed_object['field_image']['widget'][0]['#default_value']['fids']);

    return $has_image;
  }

  /**
   * Check if movie group is set.
   *
   * @param array $element
   *   Render array.
   * @param object $processed_object
   *   The object being processed.
   *
   * @return bool
   *   True if we consider the element to have a value, false otherwise.
   */
  protected function movieIsSet($element, $processed_object): bool {
    $has_movie = !empty($processed_object['field_movie_file']['widget'][0]['#default_value']['fids']);

    return $has_movie;
  }

  /**
   * Check if webpage group is set.
   *
   * @param array $element
   *   Render array.
   * @param object $processed_object
   *   The object being processed.
   *
   * @return bool
   *   True if we consider the element to have a value, false otherwise.
   */
  protected function webpageIsSet($element, $processed_object): bool {
    $has_website = !empty($processed_object['field_website']['widget'][0]['uri']['#default_value']);

    return $has_website;
  }

  /**
   * Check if scheduling rules are set.
   *
   * @param array $element
   *   Render array.
   * @param object $processed_object
   *   The object being processed.
   *
   * @return bool
   *   True if we consider the element to have a value, false otherwise.
   */
  protected function schedulingIsSet($element, $processed_object): bool {

    // N.B. the widget we use seems to require both start and end dates if
    // either is set, but I've not checked every possible combination of form
    // inputs. So let's check both.
    $has_start = $processed_object['field_scheduling_rule']['widget'][0]['start']['#default_value'];
    $has_end = $processed_object['field_scheduling_rule']['widget'][0]['end']['#default_value'];

    if ($has_start || $has_end) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Check if publishing options are set.
   *
   * @param array $element
   *   Render array.
   * @param object $processed_object
   *   The object being processed.
   *
   * @return bool
   *   True if we consider the element to have a value, false otherwise.
   */
  protected function publishingIsSet($element, $processed_object): bool {

    $published = $processed_object['status']['widget']['value']['#default_value'];
    if (!$published) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
