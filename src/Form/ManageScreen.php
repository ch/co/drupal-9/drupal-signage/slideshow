<?php

namespace Drupal\slideshow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entityqueue\Entity\EntityQueue;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Form to managed editing/adding screens.
 */
class ManageScreen extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slideshow_manage_screen';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['slideshow.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $screen_id = NULL) {
    $route = \Drupal::service('current_route_match')->getRouteName();

    $config = $this->config('slideshow.settings');

    if ($route == 'slideshow.edit_screen') {
      $edit_existing_screen = TRUE;
      $screen = $config->get('screens.' . $screen_id);
      if (!$screen) {
        throw new NotFoundHttpException();
      }
    }
    else {
      $edit_existing_screen = FALSE;
    }

    $form['#tree'] = TRUE;

    $form['title'] = [
      'name' => [
        '#type' => 'textfield',
        '#title' => 'Screen title',
        '#required' => TRUE,
      ],
      'screen_id' => [
        '#type' => 'machine_name',
        '#required' => TRUE,
        '#disabled' => $edit_existing_screen,
        '#maxlength' => 64,
        '#description' => $this->t('A unique name for this item. It must only contain lowercase letters, numbers, and underscores.'),
        '#machine_name' => [
          'exists' => [
            'Drupal\slideshow\Form\ManageScreen',
            'screenExists',
          ],
          'source' => [
            'title',
            'name',
          ],
        ],
      ],
    ];

    $form['interleave_groups'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Interleave slide groups'),
      '#description' => $this->t('If ticked, the different slide groups will be randomly interleaved. Otherwise, each slide group will be shown fully in turn.'),
      '#default_value' => TRUE,
    ];

    if ($edit_existing_screen) {
      $form['title']['name']['#default_value'] = $screen['name'];
      $form['title']['screen_id']['#default_value'] = $screen_id;
      if (isset($screen['interleave_groups']) && !$screen['interleave_groups']) {
        $form['interleave_groups']['#default_value'] = FALSE;
      }
    }

    $form['slide_group_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => 'Slide groups to show',
      '#attributes' => ['id' => 'slide-group-fieldset-wrapper'],
    ];

    if (!$form_state->get('slide_group_count')) {
      if ($edit_existing_screen) {
        $form_state->set('slide_group_count', count($screen['slide_groups']));
      }
      else {
        $form_state->set('slide_group_count', 1);
      }
    }

    for ($i = 0; $i < $form_state->get('slide_group_count'); $i++) {
      $form['slide_group_fieldset']['slide_groups'][$i] = [
        '#type' => 'select',
        '#title' => 'Slide group',
        '#options' => $this->getAllSlideGroups(),
        '#sort_options' => TRUE,
        '#required' => FALSE,
        '#empty_value' => '',
      ];

      if ($edit_existing_screen && array_key_exists($i, $screen['slide_groups'])) {
        $form['slide_group_fieldset']['slide_groups'][$i]['#default_value'] = $screen['slide_groups'][$i];
      }
    }

    $form['slide_group_fieldset']['add_more'] = [
      '#type' => 'submit',
      '#value' => 'Add another slide group to this screen',
      '#submit' => ['::addMoreSubmit'],
      '#ajax' => [
        'callback' => '::addMoreCallback',
        'wrapper' => 'slide-group-fieldset-wrapper',
      ],
    ];

    if ($edit_existing_screen) {
      $form['screen_view_link'] = [
        '#type' => 'details',
        '#title' => 'Link for Viewing screen',
        '#description' => 'To view this screen, direct your player device to: ' . Url::fromRoute('slideshow.view_screen', ['screen_id' => $screen_id], ['absolute' => TRUE])->toString(),
        '#open' => FALSE,
      ];
    }

    $form = parent::buildForm($form, $form_state);
    // The default submit action is added in parent::buildForm.  We then change
    // the title, leaving other properties unchanged.
    $form['actions']['submit']['#value'] = 'Save screen';

    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => 'Cancel',
      '#submit' => ['::cancelForm'],
    ];

    return $form;
  }

  /**
   * Submit handler for add_more button.
   *
   * @param array $form
   *   Config form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function addMoreSubmit(array &$form, FormStateInterface $form_state) {
    $slide_group_count = $form_state->get('slide_group_count') ?: 1;
    $form_state->set('slide_group_count', $slide_group_count + 1);

    $form_state->setRebuild();
  }

  /**
   * Ajax callback for add_more button.
   *
   * @param array $form
   *   Config form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['slide_group_fieldset'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('slideshow.settings');
    $screen_id = $form_state->getValue(['title', 'screen_id']);

    $config->set('screens.' . $screen_id . '.name', $form_state->getValue(['title', 'name']));

    $config->set('screens.' . $screen_id . '.interleave_groups', $form_state->getValue('interleave_groups'));

    $slide_groups = $form_state->getValue(['slide_group_fieldset', 'slide_groups']);

    $slide_groups = array_filter($slide_groups, function ($value) {
      return !is_null($value) && trim($value) !== '';
    });

    // N.B. we use array_values() here to ensure we reindex the saved array in
    // the event the preceding array_filter() has removed any blank elements.
    $config->set('screens.' . $screen_id . '.slide_groups', array_values($slide_groups));

    $config->save();
    \Drupal::service('router.builder')->rebuild();

    $form_state->setRedirect('slideshow.manage_screens');
    parent::submitForm($form, $form_state);

  }

  /**
   * Handle form cancellation.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('slideshow.manage_screens');
  }

  /**
   * Check if a screen_id exists.
   *
   * @param string $screen_id
   *   Machine name of a screen.
   *
   * @return bool
   *   True if the machine name does not exist, false otherwise
   */
  public static function screenExists(string $screen_id) {
    $exists = FALSE;
    $config = \Drupal::configFactory()->get('slideshow.settings');

    if ($config->get('screens.' . $screen_id)) {
      $exists = TRUE;
    }
    return $exists;
  }

  /**
   * Get all slide groups.
   */
  protected function getAllSlideGroups() {
    $groups = [];

    $queues = EntityQueue::loadMultipleByTargetType('node');
    if ($queues) {
      foreach ($queues as $queue) {
        $groups[$queue->id()] = $queue->label();
      }
    }

    return $groups;
  }

}
