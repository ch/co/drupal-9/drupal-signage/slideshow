<?php

namespace Drupal\slideshow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Manage display screens.
 */
class ManageScreens extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slideshow_manage_screens';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $route = \Drupal::routeMatch();
    $show_management_links = $route->getRouteObject()->getDefault('_show_management_links');

    $config = $this->config('slideshow.settings');
    $screens = $config->get('screens');

    if ($show_management_links) {
      $form['add'] = [
        '#type' => 'link',
        '#title' => 'Add display screen',
        '#url' => Url::fromRoute('slideshow.add_screen'),
        '#attributes' => [
          'class' => [
            'button',
            'button--action',
            'button--primary',
          ],
        ],
      ];
    }

    if ($screens) {
      $form['preview_help'] = [
        '#markup' => '<ul><li>The previews exclude any slide that is either unpublished, or which is only scheduled to appear in the past.</li><li>The preview of <em>currently-active slides</em> excludes slides that do not have an active schedule at the time the preview is generated.</li><li>The preview of <em>all slides</em> shows both the <em>currently-active slides</em> <strong>and</strong> those that are scheduled to appear in the future.</li>',
      ];

      $entity_queue_url = Url::fromRoute('entity.entity_queue.collection');
      $entity_queue_link = Link::fromTextAndUrl('slide group management page', $entity_queue_url)->toString();

      $form['slidegroup_help'] = [
        '#markup' => 'To see all slide assignments irrespective of their publishing or scheduling status, go to the ' . $entity_queue_link,
      ];

      $screens_table_header = [
        'title' => 'Title',
        'preview_active_link' => 'Preview currently-active slides',
        'preview_any_link' => 'Preview all slides',
      ];

      if ($show_management_links) {
        $screens_table_header += [
          'edit_link' => 'Edit',
          'delete_link' => 'Delete',
        ];
      }

      uasort($screens, function ($a, $b) {
        return strcmp($a['name'], $b['name']);
      });

      foreach ($screens as $screen_id => $screen) {
        $row = [];
        $title = $screen['name'];
        $row['title'] = $title;

        foreach (['active', 'any'] as $preview_status) {
          $preview_route_args = ['screen_id' => $screen_id, 'scheduling_status' => $preview_status];
          $preview_route = Url::fromRoute('slideshow.preview_screen', $preview_route_args);
          $preview_link = Link::fromTextAndUrl('Preview', $preview_route);

          $row['preview_' . $preview_status . '_link'] = $preview_link;
        }

        if ($show_management_links) {
          $edit_route = Url::fromRoute('slideshow.edit_screen', ['screen_id' => $screen_id]);
          $edit_link = Link::fromTextAndUrl('Edit', $edit_route);

          $delete_route = Url::fromRoute('slideshow.delete_screen', ['screen_id' => $screen_id]);
          $delete_link = Link::fromTextAndUrl('Delete', $delete_route);

          $row['edit_link'] = $edit_link;
          $row['delete_link'] = $delete_link;
        }

        $rows[] = $row;
      }

      $form['screens_table'] = [
        '#type' => 'table',
        '#header' => $screens_table_header,
        '#rows' => $rows,
      ];

    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
