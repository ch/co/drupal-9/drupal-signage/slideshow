<?php

namespace Drupal\slideshow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Generate form to delete a screen.
 */
class DeleteScreen extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slideshow_screen_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $screen_id = NULL) {
    $config = $this->config('slideshow.settings');

    $screen = $config->get('screens.' . $screen_id);
    if (!$screen) {
      throw new NotFoundHttpException();
    }

    $title = $screen['name'];

    $form['warning'] = [
      '#markup' => '</div>Are you sure you want to delete <em>' . $title . '</em>?</div>',
    ];

    $form_state->set('screen_id', $screen_id);
    $form_state->set('title', $title);

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Yes',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => 'Cancel',
      '#submit' => ['::cancelForm'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state->setRedirect('slideshow.manage_screens');

    $screen_id = $form_state->get('screen_id');

    $config = \Drupal::configFactory()->getEditable('slideshow.settings');
    $config->clear('screens.' . $screen_id);
    $config->save();

    $messenger = \Drupal::messenger();
    $messenger->addMessage($this->t('Screen %title deleted', ['%title' => $title]));

    \Drupal::service('router.builder')->rebuild();

  }

  /**
   * Form cancellation handler.
   *
   * @param array $form
   *   The config form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('slideshow.manage_screens');
  }

}
