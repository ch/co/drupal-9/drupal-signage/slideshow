<?php

namespace Drupal\slideshow\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller class for our routes based on the default content view.
 */
class ContentController extends ControllerBase {

  /**
   * The views storage service.
   *
   * @var \Drupal\views\ViewsData
   */
  protected $viewStorage;

  /**
   * Constructs a ContentController object.
   *
   * @param \Drupal\views\ViewStorageInterface $view_storage
   *   The view storage service.
   */
  public function __construct($view_storage) {
    $this->viewStorage = $view_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('view')
    );
  }

  /**
   * Displays the current content view.
   *
   * @return array
   *   Renderable array containing the view output
   */
  public function currentContent() {
    // Load the 'content' view.
    $executed_view = $this->getContentView();
    foreach ($executed_view->result as $key => $row) {
      $nid = $row->_entity->nid->value;
      if (\Drupal::service('slideshow.slide_access')->slideHasExpired($nid)) {
        unset($executed_view->result[$key]);
      }

    }
    return $executed_view->render();
  }

  /**
   * Displays the expired content view.
   *
   * @return array
   *   Renderable array containing the view output
   */
  public function expiredContent() {
    // Load the 'content' view.
    $executed_view = $this->getContentView();
    foreach ($executed_view->result as $key => $row) {
      $nid = $row->_entity->nid->value;
      if (!\Drupal::service('slideshow.slide_access')->slideHasExpired($nid)) {
        unset($executed_view->result[$key]);
      }

    }
    return $executed_view->render();
  }

  /**
   * Get the content view from core.
   *
   * @return \Drupal\views\ViewExecutable
   *   The executed core content view.
   */
  protected function getContentView() {
    // Load the 'content' view.
    $view = $this->viewStorage->load('content');

    if ($view) {
      $view_executable = $view->getExecutable();
      // Set the display ID.
      $view_executable->setDisplay('page_1');
      // Execute the view.
      $view_executable->execute();
      return $view_executable;
    }

    // Return 404 if the view is not found.
    throw new NotFoundHttpException();
  }

}
