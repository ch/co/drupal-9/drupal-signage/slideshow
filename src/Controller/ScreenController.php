<?php

namespace Drupal\slideshow\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Link;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for a screen.
 */
class ScreenController extends ControllerBase {

  /**
   * Drupal config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Slide group controller.
   *
   * @var Drupal\slideshow\Controller\SlideGroupController
   */
  protected SlideGroupController $slideGroupController;

  /**
   * Drupal renderer service.
   *
   * @var Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * Page cache kill switch.
   *
   * @var Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    SlideGroupController $slideGroupController,
    UrlGeneratorInterface $urlGenerator,
    RendererInterface $renderer,
    KillSwitch $killSwitch,
  ) {
    $this->configFactory = $configFactory;
    $this->slideGroupController = $slideGroupController;
    $this->urlGenerator = $urlGenerator;
    $this->renderer = $renderer;
    $this->killSwitch = $killSwitch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('slideshow.slidegroup_controller'),
      $container->get('url_generator'),
      $container->get('renderer'),
      $container->get('page_cache_kill_switch'),
    );
  }

  /**
   * Callback for slideshow.view_screen route.
   *
   * Either redirect to the first slide for the screen if there is one,
   * or redirect to a placeholder if not.
   *
   * @param screen $screen_id
   *   The machine name of the screen being viewed.
   */
  public function viewScreen(string $screen_id) {
    // We do not want slides cached for anonymous users.
    $this->killSwitch->trigger();

    $config = $this->configFactory->get('slideshow.settings');

    $screen = $config->get('screens.' . $screen_id);
    if (!$screen) {
      throw new NotFoundHttpException();
    }

    $slide_count = count($this->slideGroupController->getSlidesForScreen($screen_id));

    if ($slide_count > 0) {
      $route_name = 'slideshow.view_screen_position';
      $route_args = ['screen_id' => $screen_id, 'current_position' => 0];
      return new RedirectResponse($this->urlGenerator->generateFromRoute($route_name, $route_args));
    }
    else {
      $cache_key = "slideshow:$screen_id:nocontent";
      $placeholder_slide = [
        '#theme' => 'slideshow_nocontent',
        '#cache' => [
          'keys' => [
            $cache_key,
          ],
          'tags' => [
            'node_list',
          ],
        ],
        '#attached' => [
          'drupalSettings' => [
            'nextSlide' => [
              'timeout' => 10000,
              'url' => $this->urlGenerator->generateFromRoute('slideshow.view_screen', ['screen_id' => $screen_id]),
            ],
          ],
        ],
      ];

      return $placeholder_slide;
    }
  }

  /**
   * Generate preview for a given screen.
   *
   * @param string $screen_id
   *   The machine name of the screen to preview.
   * @param string $scheduling_status
   *   One of: active (only slides with a currently active schedule), any.
   */
  public function previewScreen(string $screen_id, string $scheduling_status) {

    $valid_statuses = [
      'active',
      'any',
    ];

    if (!in_array($scheduling_status, $valid_statuses)) {
      throw new NotFoundHttpException();
    }

    // We do not want slides cached for anonymous users.
    $this->killSwitch->trigger();

    $config = $this->configFactory->get('slideshow.settings');

    $screen = $config->get('screens.' . $screen_id);
    if (!$screen) {
      throw new NotFoundHttpException();

    }

    $user = User::load(\Drupal::currentUser()->id());

    $label = $screen['name'];
    $now = new DrupalDateTime('now');
    $formatted_date = $now->format('F j Y H:i');

    $slides = $this->slideGroupController->getSlidesForScreen($screen_id);

    $content = [];

    if ($scheduling_status == 'active') {
      $header_text = "Preview of currently-active slides for <em>$label</em>";
    }
    else {
      $header_text = "Preview of current and future slides for <em>$label</em>";
    }

    $content['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => "$header_text (generated at $formatted_date)",
    ];

    if ($user->hasPermission('bypass node access')) {
      $content['warning'] = [
        '#markup' => '<p><strong>Warning:</strong> you have the <em>bypass node access</em> permission, so this preview can include slides that are not visible to most users.</p>',
      ];
    }

    $content['slides'] = [];

    for ($i = 0; $i < count($slides); $i++) {
      $preview_args = ['screen_id' => $screen_id, 'current_position' => $i, 'scheduling_status' => $scheduling_status];
      $src_callback = $this->urlGenerator->generateFromRoute('slideshow.preview_screen_position_json', $preview_args);

      $node = Node::load($slides[$i]);

      $can_edit = $node->access('update', $user);

      if ($can_edit) {
        $title_link = Link::createFromRoute(
          $node->getTitle(),
          'entity.node.edit_form',
          ['node' => $slides[$i]]
        )->toRenderable();
      }
      else {
        $title_link = [
          '#markup' => $node->getTitle(),
        ];
      }

      $content['slides'][$i] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['slide-preview-container'],
          'data-preview-src-callback' => $src_callback,
        ],
        'slide_title_container' => [
          '#type' => 'container',
          'title_link' => $title_link,
        ],
        'preview_iframe' => ['#markup' => '<div class="iframe-placeholder">Loading preview...</div>'],
      ];

    }

    if (count($slides) == 0) {
      $content['noslides'] = [
        '#markup' => 'There are no slides currently visible on this screen',
      ];
    }

    $content['#attached']['library'][] = 'slideshow/previewslide';
    $content['#attached']['library'][] = 'slideshow/load-previewslide';
    $content['#cache'] = [
      'max-age' => 0,
    ];
    return $content;
  }

  /**
   * Generate a preview iframe for a given src.
   *
   * @param string $src
   *   Iframe src URL.
   *
   * @return array
   *   Render array
   */
  protected function generatePreviewIframe(string $src): array {
    return [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'src' => $src,
        'loading' => 'eager',
        'class' => ['slide-preview'],
      ],
    ];
  }

  /**
   * Generate slide preview as json.
   */
  public function previewScreenPositionJson($screen_id, $current_position, $scheduling_status): JsonResponse {
    $preview_args = [
      'screen_id' => $screen_id,
      'current_position' => $current_position,
      'scheduling_status' => $scheduling_status,
    ];

    $src = $this->urlGenerator->generateFromRoute('slideshow.preview_screen_position', $preview_args);
    $preview = $this->generatePreviewIframe($src);

    return new JsonResponse([
      'data' => $this->renderer->renderRoot($preview),
    ]);
  }

}
