<?php

namespace Drupal\slideshow\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\Entity\User;

/**
 * Controller to check slide access permissions.
 */
class SlideAccessController {
  /**
   * Node storage manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $nodeStorage;

  /**
   * Construct the service object.
   */
  public function __construct(EntityTypeManagerInterface $entityManager) {
    $this->nodeStorage = $entityManager->getStorage('node');
  }

  /**
   * Check if the current user has a requested slide permission.
   *
   * @param int|EntityInterface $slide
   *   The slide, or perhaps its entity ID.
   * @param string $op
   *   The requested operation ('view', 'update', 'create', 'delete')
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The resulting access outcome.
   */
  public function checkAccess(int|EntityInterface $slide, string $op = 'view'): AccessResult {
    if (!$slide instanceof EntityInterface) {
      $slide = $this->nodeStorage->load($slide);
    }

    if (!$slide) {
      return AccessResult::neutral();
    }
    if ($op == 'view') {
      $current_route = \Drupal::routeMatch()->getRouteName();
      $route_parameters = \Drupal::routeMatch()->getParameters();

      $scheduling_status = $route_parameters->get('scheduling_status', 'active');

      $is_current = $this->slideIsScheduledNow($slide);
      $has_expired = $this->slideHasExpired($slide);

      // Special-case a couple of routes where we want to grant view access if
      // the user has update rights:
      //
      // entity.node.canonical : normal route for viewing a node (i.e.
      // /node/$nid).  We only expect/want content editors to access this
      // route.
      //
      // slideshow.preview_slide : we use an embedded iframe that loads this
      // route as part of rendering /node/$nid.
      //
      // entity.entity_subqueue.canonical : form used by entityqueue to manage
      // queue itemss, which shows node titles. If a user has permission to
      // edit a queue, we are happy for them to see the content assigned to it.
      $useUpdatePermissionsForRoute = [
        'entity.node.canonical',
        'slideshow.preview_slide',
        'entity.entity_subqueue.canonical',
      ];

      if (in_array($current_route, $useUpdatePermissionsForRoute)) {
        return AccessResult::forbiddenIf(!$slide->access('update'));
      }

      if ($scheduling_status == 'active') {
        return AccessResult::forbiddenIf(!$is_current)->setCacheMaxAge(0);
      }
      elseif ($scheduling_status == 'any') {
        if ($has_expired) {
          return AccessResult::forbidden()->setCacheMaxAge(0);
        }
        else {
          $user = User::load(\Drupal::currentUser()->id());
          $has_permission = AccessResult::allowedIfHasPermission($user, 'bypass slideshow scheduling_status restriction');
          return AccessResult::forbiddenIf(!$has_permission->isAllowed());
        }
      }
      else {
        // Unknown scheduling status. Should not reach this point
        // because we expect the routing controller to validate this,
        // but lets be sure.
        return AccessResult::forbidden();
      }

    }

    if ($op == 'view label') {
      return AccessResult::allowedIf($slide->access('update'));
    }

    // Default fallback.
    return AccessResult::neutral();

  }

  /**
   * Check if a slide is scheduled to appear now.
   *
   * Note that this will return False if two conditions are both met:
   * 1. The slide has a scheduling rule.
   * 2. The scheduling rule does not match the current time.
   * In particular, a slide with no scheduling rule defined is considered to
   * always be scheduled.
   *
   * @param int|EntityInterface $slide
   *   Slide Entity, or the ID of one.
   *
   * @return bool
   *   False if the slide has a rule that is not current, True otherwise.
   */
  protected function slideIsScheduledNow(int|EntityInterface $slide): bool {
    if (!$slide instanceof EntityInterface) {
      $slide = $this->nodeStorage->load($slide);
    }

    if (!$this->slideHasSchedulingRule($slide)) {
      // Slides without a scheduling rule are deemed to always be
      // available.
      return TRUE;
    }

    $now = new \DateTime();
    $ruleHelper = $slide->field_scheduling_rule[0]->getHelper();

    $nextOccurrence = $ruleHelper->generateOccurrences($now, NULL)->current();

    if (!$nextOccurrence) {
      return FALSE;
    }

    $nextStart = $nextOccurrence->getStart();
    $nextEnd = $nextOccurrence->getEnd();

    return ($nextStart < $now && $nextEnd > $now);

  }

  /**
   * Check if a slide's scheduling has expired.
   *
   * @param int|EntityInterface $slide
   *   Slide entity, or the ID of one.
   *
   * @return bool
   *   True if all scheduled appearances have past.
   */
  public function slideHasExpired(int|EntityInterface $slide): bool {
    if (!$slide instanceof EntityInterface) {
      $slide = $this->nodeStorage->load($slide);
    }

    if (!$this->slideHasSchedulingRule($slide)) {
      // Slides without a scheduling rule cannot have expired.
      return FALSE;
    }

    $now = new \DateTime();
    $ruleHelper = $slide->field_scheduling_rule[0]->getHelper();

    $nextOccurrence = $ruleHelper->generateOccurrences($now, NULL)->current();

    if (!$nextOccurrence) {
      return TRUE;
    }

    // Deem the slide to have not expired if we have no reason
    // to think it has.
    return FALSE;

  }

  /**
   * Check if a slide has a scheduling rule.
   *
   * @param int|EntityInterface $slide
   *   Slide Entity, or the ID of one.
   *
   * @return bool
   *   TRUE if the slide has a scheduling rule, FALSE if not.
   */
  protected function slideHasSchedulingRule(int|EntityInterface $slide): bool {
    if (!$slide instanceof EntityInterface) {
      $slide = $this->loadSlide($slide);
    }
    return $slide->hasField('field_scheduling_rule') && !($slide->field_scheduling_rule->isEmpty());
  }

}
