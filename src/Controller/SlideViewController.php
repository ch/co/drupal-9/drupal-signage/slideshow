<?php

namespace Drupal\slideshow\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Slide view controller.
 */
class SlideViewController extends ControllerBase {

  /**
   * Slide group controller.
   *
   * @var Drupal\slideshow\Controller\SlideGroupController
   */
  protected SlideGroupController $slideGroupController;

  /**
   * URL Generator service.
   *
   * @var Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected UrlGeneratorInterface $urlGenerator;

  /**
   * Page cache kill switch.
   *
   * @var Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * Constructor.
   *
   * @param Drupal\slideshow\Controller\SlideGroupController $slideGroupController
   *   Slide group controller.
   * @param Drupal\Core\Routing\UrlGeneratorInterface $urlGenerator
   *   Url generator service.
   * @param Drupal\Core\PageCache\ResponsePolicy\KillSwitch $killSwitch
   *   Page cache kill switch.
   */
  public function __construct(
    SlideGroupController $slideGroupController,
    UrlGeneratorInterface $urlGenerator,
    KillSwitch $killSwitch,
  ) {
    $this->slideGroupController = $slideGroupController;
    $this->urlGenerator = $urlGenerator;
    $this->killSwitch = $killSwitch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('slideshow.slidegroup_controller'),
      $container->get('url_generator'),
      $container->get('page_cache_kill_switch'),
    );
  }

  /**
   * Preview a slide entity.
   *
   * @param int $slide_id
   *   Entity ID of the slide to preview.
   * @param string $scheduling_status
   *   One of: active (only slides with a currently active schedule), any.
   *
   * @return array
   *   Render array.
   */
  public function previewSlide(int $slide_id, string $scheduling_status = 'active') {
    $node = Node::load($slide_id);
    if ($node) {
      return $this->renderSlidePreview($node);
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * View slide at a given position for a screen.
   */
  public function viewScreenPosition($screen_id, $current_position) {
    // The response returned by this controller changes regularly, and we want
    // changes to be immediately visible: so, do not cache.
    $this->killSwitch->trigger();

    $slide_id = $this->slideGroupController->getSlideId($screen_id, $current_position);
    $next_position = $this->slideGroupController->getPositionAfter($screen_id, $current_position);

    $next_url_args = ['screen_id' => $screen_id, 'current_position' => $next_position];
    $next_url = Url::fromRoute('slideshow.view_screen_position', $next_url_args);

    $node = Node::load($slide_id);

    // Check if the node exists.
    if ($node) {
      $content = $this->renderSlide($node);

      $attachments = $this->attachmentsForSlide($node);
      $content['#attached'] = $attachments;

      $nextSlideSettings = $this->nextSlideSettings($node, $next_url);
      $content['#attached']['drupalSettings']['nextSlide'] = $nextSlideSettings;

      return $content;
    }
    else {
      // Return to starting point for the screen if we have a problem showing
      // the slide.
      $route_name = 'slideshow.view_screen';
      $route_args = ['screen_id' => $screen_id];
      return new RedirectResponse($this->urlGenerator->generateFromRoute($route_name, $route_args));
    }
  }

  /**
   * Preview slide at a given position for a screen.
   */
  public function previewScreenPosition($screen_id, $current_position, $scheduling_status) {
    $slide_id = $this->slideGroupController->getSlideId($screen_id, $current_position);

    $node = Node::load($slide_id);

    // Check if the node exists.
    if ($node) {
      $content = $this->renderSlidePreview($node);
    }
    else {
      $content = [
        '#markup' => "error previewing slide $current_position for $screen_id",
      ];
    }
    return $content;
  }

  /**
   * Given a slide sntity, return a render array for the slide preview.
   *
   * @param \Drupal\node\Entity\Node $entity
   *   The slide to preview.
   *
   * @return array
   *   Render array of the previewed slide.
   */
  public function renderSlidePreview(Node $entity) {
    $content = $this->renderSlide($entity);
    $content['#view_mode'] = 'preview';
    $attachments = $this->attachmentsForSlide($entity);
    $content['#attached'] = $attachments;

    return $content;
  }

  /**
   * Given a slide entity, return a render array.
   *
   * @param \Drupal\node\Entity\Node $entity
   *   The slide to render.
   */
  public function renderSlide(Node $entity) {
    // We do not want slides cached for anonymous users.
    $this->killSwitch->trigger();

    // Get the node's bundle (content type).
    $bundle = $entity->getType();

    if (!$entity->access('view')) {
      throw new AccessDeniedHttpException();
    }

    // Check the bundle to determine the template.
    switch ($bundle) {
      case 'page':
        $template = 'slideshow_slide_page';

        $content = [
          '#theme' => $template,
          '#node' => $entity,
          '#main_heading' => $entity->get('field_main_heading')->value,
          '#text_content' => $entity->get('field_text_content')->value,
          '#slide_image' => $entity->get('field_image'),
          '#image_size' => $entity->get('field_image_size')->value,
        ];

        break;

      case 'webpage_slide':
        $template = 'slideshow_webpage';

        $content = [
          '#theme' => $template,
          '#webpage_uri' => $entity->get('field_website')->uri,
        ];

        break;

      case 'movie_slide':
        $template = 'slideshow_movie';

        $content = [
          '#theme' => $template,
          '#movie_file' => $entity->get('field_movie_file')->first(),
          '#preview_time' => $entity->get('field_preview_timevalue')->value,
        ];

        break;

      default:
        $template = 'slideshow_slide_default';
        $content = [
          '#theme' => $template,
          '#node' => $entity,
        ];

        break;
    }

    // Disable content caching. Our content is simple, and it's
    // non-trivial to determine cachability given that slides
    // have scheduling rules.
    $content['#cache'] = [
      'max-age' => 0,
    ];

    return $content;

  }

  /**
   * Generate attachments for a slide.
   *
   * @param \Drupal\node\Entity\Node $entity
   *   The slide node entity of interest.
   *
   * @return array
   *   Array for populating the '#attachments' render array element
   */
  public function attachmentsForSlide(Node $entity) {
    $bundle = $entity->getType();

    $attachments = [];

    switch ($bundle) {
      case 'page':

        $imageBlur = $entity->get('field_image_blur')->value;
        $imageOpacity = $entity->get('field_image_opacity')->value;

        $bgColour = $entity->get('field_background_colour')->color;
        $textColour = $entity->get('field_text_colour')->color;
        $textBoxColour = $entity->get('field_textbox_colour')->color;

        if ($imageBlur) {
          $attachments['drupalSettings']['imageeffects']['blur'] = $imageBlur;
        }

        if ($imageOpacity) {
          $attachments['drupalSettings']['imageeffects']['opacity'] = $imageOpacity;
        }

        if ($bgColour) {
          $attachments['drupalSettings']['slideproperties']['bgcolour'] = $bgColour;
        }

        if ($textColour) {
          $attachments['drupalSettings']['slideproperties']['textcolour'] = $textColour;
        }

        if ($textBoxColour) {
          $attachments['drupalSettings']['slideproperties']['textboxcolour'] = $textBoxColour;
        }
        break;

      case 'movie_slide':
        break;

      case 'webpage_slide':
        break;
    }

    return $attachments;
  }

  /**
   * Generate nextslide js settings for a slide.
   */
  public function nextSlideSettings(Node $entity, Url $next_url) {
    $bundle = $entity->getType();

    $settings = [];

    switch ($bundle) {
      case 'page':
      case 'webpage_slide':
        // Web form uses seconds, which we convert to ms for the javascript.
        $settings['timeout'] = $entity->get('field_display_time')->value * 1000;
        break;
    }

    $settings['url'] = $next_url->toString();

    return $settings;

  }

}
