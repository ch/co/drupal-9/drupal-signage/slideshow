<?php

namespace Drupal\slideshow\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entityqueue\Entity\EntitySubqueue;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for slide groups.
 */
class SlideGroupController {

  /**
   * Node storage manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $nodeStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly EntityTypeManagerInterface $entityManager,
  ) {
    $this->nodeStorage = $entityManager->getStorage('node');
  }

  /**
   * Get ID (nid) of a slide.
   *
   * @param string $screen_id
   *   Machine name of the screen.
   * @param int $current_position
   *   Slide position being requested.
   *
   * @return int|null
   *   The nid of the requested slide, or null if not found.
   */
  public function getSlideId(string $screen_id, int $current_position): int|null {
    $slide_ids = $this->getSlidesForScreen($screen_id);

    if ($current_position < count($slide_ids)) {
      $slide_id = $slide_ids[$current_position];
    }
    else {
      $slide_id = $slide_ids[0];
    }

    return $slide_id;
  }

  /**
   * Get the position index of the next slide.
   *
   * @param string $screen_id
   *   Machine name of the screen.
   * @param int $current_position
   *   The current slide position.
   *
   * @return int
   *   The position index of the next slide to show
   */
  public function getPositionAfter(string $screen_id, int $current_position): int {
    $slide_count = count($this->getSlidesForScreen($screen_id));
    $next_position = $current_position + 1;

    // We index positions on the zero-based array so reset
    // to zero if we hit the slide_count.
    if ($next_position >= $slide_count) {
      $next_position = 0;
    }
    return $next_position;
  }

  /**
   * Return list of slide ids for a given group.
   */
  public function getSlidesForGroup(string $slide_group) {
    $slide_ids = [];

    $queue = EntitySubqueue::load($slide_group);

    if ($queue) {
      $items = $queue->get('items')?->getValue();
      foreach ($items as $item) {
        $slide_id = $item['target_id'];
        $slide = $this->loadSlide($slide_id);
        if ($slide->access('view') && $slide->isPublished()) {
          $slide_ids[] = $slide_id;
        }
      }
    }
    return $slide_ids;

  }

  /**
   * Get array of all slides for a screen.
   *
   * @param string $screen_id
   *   Machine name of the screen.
   *
   * @return array
   *   Array of slides, as $slide_position => $slide_nid
   */
  public function getSlidesForScreen(string $screen_id): array {
    $config = $this->configFactory->get('slideshow.settings');

    $screen = $config->get('screens.' . $screen_id);
    if (!$screen) {
      throw new NotFoundHttpException();
    }

    $slides = [];
    $groups = $screen['slide_groups'];

    $interleave = $screen['interleave_groups'];

    if ($groups) {
      if ($interleave) {
        foreach ($groups as $group) {
          $grouped_slides[] = $this->getSlidesForGroup($group);
        }
        $slides = $this->interleaveSlideGroups($grouped_slides, $screen_id);
      }
      else {
        foreach ($groups as $group) {
          $slides = array_merge($slides, $this->getSlidesForGroup($group));
        }
      }
    }

    // The array of slides needs to be $position => $nid. We thus use
    // array_values() to reset the keys rather than using the original
    // positions of any potentially exclusive slides.
    $exclusive_slides = array_values(array_filter($slides, [$this, 'slideIsExclusive']));
    if ($exclusive_slides) {
      return $exclusive_slides;
    }
    else {
      return $slides;
    }

  }

  /**
   * Randomly interleave slides, but preserving group order.
   *
   * Suppose we have two groups:
   * $a = [1, 2, 3];
   * $b = [4, 5];
   * These are passed as an array of arrays, with an implicit incrementing
   * key:
   * $slide_groups = [ 0 => $a, 1 => $b];
   *  The algorithm is then:
   *
   * 1. use $position to keep track of the index of which slide to include next
   *    for each group.
   * 2. produce a list of which group to draw the nth slide for the result from,
   *     e.g. $groups = [0, 0, 0, 1, 1];
   * 3. shuffle $groups with a predictable seed.
   * 4. iterate through (shuffled) $groups to draw the next slide from whichever
   *    group we want next, incrementing the relevant $position as we go.
   *
   * @param array $grouped_slides
   *   Array of arrays of slide IDs.
   * @param string $screen_name
   *   Machine name of screen.
   *
   * @return array
   *   Array of slide IDs.
   */
  protected function interleaveSlideGroups(array $grouped_slides, string $screen_name) {
    // We just want some way to simply convert a string to an integer that
    // can be used as a seed. crc32 works fine.
    srand(crc32($screen_name));

    $interleaved_slides = [];

    // Keep track of which position we are up to in each array.
    $position = array_fill(0, count($grouped_slides), 0);

    $groups = [];
    foreach ($grouped_slides as $group => $slides) {
      for ($i = 0; $i < count($slides); $i++) {
        $groups[] = $group;
      }
    }

    shuffle($groups);

    foreach ($groups as $group) {
      $interleaved_slides[] = $grouped_slides[$group][$position[$group]];
      $position[$group]++;
    }

    return $interleaved_slides;
  }

  /**
   * Load slide (node) by id.
   *
   * @param int $slide_id
   *   Slide id (nid)
   *
   * @return Drupal\node\NodeInterface
   *   Slide node entity
   */
  protected function loadSlide(int $slide_id) {
    $node = $this->nodeStorage->load($slide_id);

    return $node;
  }

  /**
   * Check if a slide has an exclusive schedule.
   *
   * @param int $slide_id
   *   Slide id (nid)
   *
   * @return bool
   *   TRUE if the slide schedule is set as exclusive
   */
  protected function slideIsExclusive(int $slide_id) {
    $slide = $this->loadSlide($slide_id);

    return ($slide->hasField('field_schedule_exclusive') &&
    !empty($slide->field_schedule_exclusive->value) &&
    $slide->field_schedule_exclusive->value == 1);
  }

}
